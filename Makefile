build: Dockerfile
	docker build -t zed .

run:
	docker run --gpus all -it --privileged zed

zed:
	docker run --gpus all -it --privileged stereolabs/zed:3.1-ros-devel-cuda10.0-ubuntu18.04